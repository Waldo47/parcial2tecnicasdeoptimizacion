﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradingRoutesSimulation
{
    public class Merchant
    {
        public readonly Point Town;
        public readonly Point CapitalCity;

        public Point Position;

        public bool GoingToCapital = true;


        Point[] pathgo,pathreturn;
        List<Point> currentPath;
        Pathfinding pathfinder;



        public Merchant(Point position, Point capital, TerrainType[,] map)
        {
            Town = Position = position;
            CapitalCity = capital;
            //target = GoingToCapital ? CapitalCity : Town;


            pathfinder = new Pathfinding(map);
            pathgo = pathfinder.GetPath(Position, CapitalCity).ToArray();
            pathreturn = pathfinder.GetPath(CapitalCity, Town).ToArray();
            currentPath = pathgo.ToList();
        }

        public void UpdateOn(TerrainType[,] map)
        {
            //Point target = GoingToCapital ? CapitalCity : Town;
            //var pathfinder = new Pathfinding(map);
            //var path = pathfinder.GetPath(Position, targetCity).Skip(1);
            //var pathNow = pathfinder.GetPath(Position, targetCity).Skip(1);


            if (currentPath.Any())
            {
                Position = currentPath.First();
                currentPath.RemoveAt(0);
            }
            else
            {

                if (GoingToCapital)
                {
                    currentPath = pathreturn.ToList();
                    GoingToCapital = !GoingToCapital;
                }
                else
                {
                    currentPath = pathgo.ToList();
                    GoingToCapital = !GoingToCapital;
                }
            }
        }
    }
}
